module.exports = {
  extends: [
    'airbnb',
    'prettier',
    'prettier/react',
    'plugin:prettier/recommended',
    'eslint-config-prettier',
    '@react-native-community',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['prettier', 'import'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
        alias: {
          assets: './src/assets',
          components: './src/Components',
          Atoms: './src/Components/Atoms',
          Molecules: './src/Components/Molecules',
          Organisms: './src/Components/Organisms',
          Pages: './src/Pages',
          styles: './src/styles',
          Utils: './src/Utils',
        },
      },
    },
  },
  rules: {
    'import/no-cycle': 'off',
    'no-useless-catch': 'off',
    'react/prefer-stateless-function': 'off',
    'array-callback-return': 'off',
    'consistent-return': 'off',
    'react/no-array-index-key': 'off',
    'react/state-in-constructor': 'off',
    'import/no-unresolved': 'off',
    'comma-dangle': 'off',
    'react/jsx-props-no-spreading': 'off',
    'no-debugger': 'error',
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js'],
      },
    ],
    'import/prefer-default-export': 'off',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        printWidth: 120,
        bracketSpacing: true,
      },
    ],
    'jsx-a11y/label-has-associated-control': 'off',
    'import/no-extraneous-dependencies': ['error', { packageDir: './' }],
    'no-console': ['error', { allow: ['warn', 'error', 'tron'] }],
    'jsx-a11y/click-events-have-key-events': 'off',
  },
};
