import React, { Suspense } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import './App.scss';
import { Router } from 'react-router-dom';
import { store, persistor } from './Store/rootReducer';
import history from './Utils/history';
import Routes from './Routers';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Suspense fallback={<div>loading</div>}>
          <Router history={history}>
            <Routes />
          </Router>
        </Suspense>
      </PersistGate>
    </Provider>
  );
}

export default App;
