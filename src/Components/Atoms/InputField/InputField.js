import React from 'react';

import './InputField.scss';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useField } from 'formik';

const InputField = ({ ...props }) => {
  const [field, meta] = useField(props);
  const { size, type, label, alertText, placeholder, onChange, disabled, autocomplete } = props;
  const hasError = meta.touched && meta.error;

  return (
    <Form.Group controlId={label}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        disabled={disabled}
        name={field.name}
        size={size}
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        autoComplete={autocomplete}
      />
      {hasError && <Form.Text className="error">{alertText}</Form.Text>}
    </Form.Group>
  );
};

InputField.defaultProps = {
  size: '',
  label: '',
  type: 'text',
  alertText: '',
  disabled: false,
  placeholder: '',
  error: false,
};

InputField.propTypes = {
  label: PropTypes.string,
  alertText: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  size: PropTypes.oneOf(['lg', 'sm', '']),
  type: PropTypes.oneOf(['text', 'email', 'password', 'date']),
};

export default InputField;
