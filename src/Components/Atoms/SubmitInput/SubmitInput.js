import React from 'react';
import PropTypes from 'prop-types';
import './SubmitInput.scss';
import { Button } from 'react-bootstrap';

export default function SubmitInput({ value, type }) {
  return (
    <Button type="submit" className={`${type} submit`}>
      {value}
    </Button>
  );
}

SubmitInput.propTypes = {
  value: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['primary']).isRequired,
};
