import React from 'react';
import { Button, Col } from 'react-bootstrap';
import play from '../../../Assets/Images/play.svg';
import './FeaturedMovieDetails.scss';

export default function FeaturedMovieDetails({ title, description }) {
  return (
    <div className="featured-movie-details-container">
      <Col xs={12} className="title">
        {title}
      </Col>
      <Col xs={12} className="description">
        {description}
      </Col>
      <Col xs={12} className="actions">
        <Button className="button-play">
          شاهد الآن <img src={play} className="play" alt="" />
        </Button>
        <Button className="button-info">i</Button>
      </Col>
    </div>
  );
}
