import React from 'react';
import { Col } from 'react-bootstrap';
import FeaturedMovieDetails from '../../Molecules/FeaturedMovieDetails/FeaturedMovieDetails';
import './FeaturedMovie.scss';

export default function FeaturedMovie({ title, description, image }) {
  return (
    <div
      className="featured-movie-container"
      style={{
        backgroundImage: `url("https://image.tmdb.org/t/p/original/${image}")`,
      }}
    >
      <Col xs={12} sm={4}>
        <FeaturedMovieDetails title={title} description={description} />
      </Col>
    </div>
  );
}
