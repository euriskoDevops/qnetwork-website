import React from 'react';
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import { Alert, Row, Col } from 'react-bootstrap';
import validation from './validation';
import InputField from '../../Atoms/InputField/InputField';
import SubmitInput from '../../Atoms/SubmitInput/SubmitInput';

import './LoginForm.scss';

export default function LoginForm({ onSubmit, loginError }) {
  const { t } = useTranslation();
  return (
    <div className="login-form">
      <Formik
        validationSchema={validation}
        initialValues={{ username: '', password: '' }}
        onSubmit={(props) => onSubmit(props)}
      >
        {({ handleSubmit, handleChange, errors }) => {
          return (
            <Form noValidate onSubmit={(e) => handleSubmit(e)}>
              <Row className="form-container">
                <Col xs={12}>
                  <InputField
                    name="username"
                    label={t('globals.username')}
                    type="text"
                    placeholder={t('globals.username')}
                    onChange={(e) => handleChange(e)}
                    alertText={errors.username}
                    error={!!errors.username}
                  />
                </Col>
                <Col xs={12}>
                  <InputField
                    name="password"
                    label={t('globals.password')}
                    type="password"
                    placeholder={t('globals.password')}
                    onChange={(e) => handleChange(e)}
                    alertText={errors.password}
                    error={!!errors.password}
                  />
                </Col>
                {loginError && (
                  <Col xs={12} className="login-error">
                    <Alert variant="danger">{loginError}</Alert>
                  </Col>
                )}
                <Col xs={12} className="d-flex flex-row-reverse info-link">
                  {t('globals.forgetPassword')}
                </Col>
                <SubmitInput value={t('globals.submitLogin')} type="primary" />
                <Col xs={12} className="register">
                  {t('globals.forgetPassword')}
                </Col>
              </Row>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}
