import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';

import './MoviesList.scss';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

export default function MovieList({ movies, sectionTitle }) {
  return (
    <>
      <h1 className="movie-section-title">{sectionTitle}</h1>
      <Swiper
        className="movies-list-container"
        spaceBetween={20}
        navigation={false}
        grabCursor={false}
        pagination={false}
        draggable
        loop
        breakpoints={{
          1378: {
            slidesPerView: 6,
            slidesPerGroup: 6,
          },
          998: {
            slidesPerView: 5,
            slidesPerGroup: 5,
          },
          625: {
            slidesPerView: 4,
            slidesPerGroup: 4,
          },
          0: {
            slidesPerView: 3,
            slidesPerGroup: 3,
          },
        }}
        preventClicksPropagation
        preventClicks
        scrollbar={{ draggable: false, hide: true }}
        slideToClickedSlide={false}
      >
        {movies.map(({ title, image, description, id }, idx) => (
          <SwiperSlide key={idx} className="movie">
            <img src={`https://image.tmdb.org/t/p/original/${image}`} className="movie-image" alt="" />
            <div className="movie-title">{title}</div>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
