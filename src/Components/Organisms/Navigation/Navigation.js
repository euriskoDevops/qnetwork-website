import React from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import logo from '../../../Assets/Images/logo.png';
import profile from '../../../Assets/Images/profile.jpeg';
import notification from '../../../Assets/Images/notifications.svg';
import search from '../../../Assets/Images/search.svg';
import './Navigation.scss';
import { clearAuthentication } from '../../../Store/Actions/AuthActions';

export default function Navigation() {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const onLogout = () => {
    dispatch(clearAuthentication());
  };

  return (
    <Navbar expand="lg" className="main-navigation sidebar-nav" fixed="top">
      <Navbar.Brand href="#home">
        <img src={logo} width={52} alt="" />
      </Navbar.Brand>
      <Navbar.Toggle data-toggle="offcanvas" />
      <Navbar.Collapse id="basic-navbar-nav" className="justify-content-between align-items-center w-100">
        <Nav className="mx-auto text-md-center text-left">
          <Nav.Link href="#home">{t('landing.main')}</Nav.Link>
          <Nav.Link href="#home">{t('landing.series')}</Nav.Link>
          <Nav.Link href="#link">{t('landing.programs')}</Nav.Link>
          <Nav.Link href="#link">{t('landing.preferences')}</Nav.Link>
        </Nav>
      </Navbar.Collapse>
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="#home">
            <img className="menu-icon" src={search} alt="" />
          </Nav.Link>
          <Nav.Link href="#home">
            <img className="menu-icon" src={notification} alt="" />
          </Nav.Link>
          <NavDropdown title={<img className="thumbnail-image" src={profile} alt="user pic" />} id="basic-nav-dropdown">
            <NavDropdown.Item onClick={() => onLogout()}>{t('globals.logout')}</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
