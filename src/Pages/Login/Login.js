import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Col, Container, Row } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { authenticate } from '../../Services/Authentication';
import { saveAuthentication } from '../../Store/Actions/AuthActions';
import LoginForm from '../../Components/Organisms/LoginForm/LoginForm';
import Loader from '../../Components/Atoms/Loader/Loader';

import logo from '../../Assets/Images/logo.png';
import './Login.scss';

export default function Login() {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const dispatch = useDispatch();

  const submitLogin = async (user) => {
    try {
      setLoading(true);
      const authenticateUser = await authenticate(user.username, user.password);
      authenticateUser.isAuthenticated = true;
      dispatch(saveAuthentication(authenticateUser));
      setError('');
      setLoading(false);
    } catch (e) {
      setLoading(false);
      setError(t('globals.invalidCredentials'));
    }
  };

  return (
    <Container fluid className="login-page">
      <Row className="justify-content-md-center">
        <Col sm={4} xs={12}>
          <img src={logo} alt="" className="logo" />
        </Col>
      </Row>
      <Row className="justify-content-md-center">
        <Col sm={4} xs={12}>
          <div className="title">{t('globals.login')}</div>
        </Col>
      </Row>
      <Row className="justify-content-md-center login-form-container">
        <Col sm={4} xs={12}>
          <LoginForm onSubmit={(user) => submitLogin(user)} loginError={error} />
          {loading && <Loader />}
        </Col>
      </Row>
    </Container>
  );
}
