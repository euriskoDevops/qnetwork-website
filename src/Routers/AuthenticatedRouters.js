import React from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import Navigation from '../Components/Organisms/Navigation/Navigation';
import Home from '../Pages/Home/Home';

export default function DashboardRouters() {
  return (
    <div>
      <Navigation />
      <Switch>
        <Redirect exact from="/" to="/browse" />
        <Route path="/browse" component={Home} />
      </Switch>
    </div>
  );
}
