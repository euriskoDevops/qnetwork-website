import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import Login from '../Pages/Login/Login';
import AuthenticatedRouters from './AuthenticatedRouters';

export default function Routes() {
  const { i18n } = useTranslation();
  document.body.dir = i18n.dir();
  const { isAuthenticated } = useSelector((state) => state.authUser);

  return (
    <Switch>
      {!isAuthenticated ? (
        <Route>
          <Login />
        </Route>
      ) : (
        <Redirect from="/auth" to="/" />
      )}
      {!isAuthenticated ? (
        /* Redirect to `/auth` when user is not authorized */
        <Redirect to="/auth/login" />
      ) : (
        <AuthenticatedRouters />
      )}
    </Switch>
  );
}
