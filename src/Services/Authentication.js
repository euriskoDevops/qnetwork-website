import { publicHttpClient } from '../Utils/httpClient';

/**
 * @param {string} username
 * @param {string} password
 */
export const authenticate = async (username, password) => {
  try {
    const body = {
      username,
      password,
    };
    const { data } = await publicHttpClient.post('user/authenticate', body);
    return data;
  } catch (e) {
    throw e.response;
  }
};
