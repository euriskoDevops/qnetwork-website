import { CHANGE_AUTH, CLEAR_AUTHENTICATION, SAVE_AUTHENTICATION } from '../constants';

const saveAuthentication = (data) => ({
  type: SAVE_AUTHENTICATION,
  payload: data,
});

const clearAuthentication = () => ({
  type: CLEAR_AUTHENTICATION,
  payload: null,
});

const changeAuth = (result) => {
  return {
    type: CHANGE_AUTH,
    payload: { result },
  };
};

export { changeAuth, clearAuthentication, saveAuthentication };
