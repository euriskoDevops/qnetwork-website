import { CHANGE_AUTH, CLEAR_AUTHENTICATION, SAVE_AUTHENTICATION } from '../../constants';

const INITIAL_STATE = {
  isAuthenticated: false,
  access_token: null,
  expires_in: null,
  token_type: null,
  scope: null,
  refresh_token: null,
  refresh_expires_in: null,
};

const setUserAuthentication = (state, action) => ({
  ...state,
  isAuthenticated: action.payload.result,
});

function authUserReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SAVE_AUTHENTICATION:
      return {
        ...state,
        ...action.payload,
      };
    case CLEAR_AUTHENTICATION:
      return INITIAL_STATE;
    case CHANGE_AUTH:
      return setUserAuthentication(state, action);
    default:
      return state;
  }
}
export default authUserReducer;
