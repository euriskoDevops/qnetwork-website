import { combineReducers } from 'redux';

import authUserReducer from './AuthReducer/authUserReducer';

export default combineReducers({
  authUser: authUserReducer,
});
