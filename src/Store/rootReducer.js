import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import rootReducer from './Reducers';

const persistConfig = {
  key: 'root',
  storage,
};
const pReducer = persistReducer(persistConfig, rootReducer);
const middleware = [thunk, logger];
let s;
if (process.env.NODE_ENV === 'development') {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  s = composeEnhancers(applyMiddleware(...middleware))(createStore)(pReducer);
} else {
  s = compose(applyMiddleware(...middleware))(createStore)(pReducer);
}
const store = s;
const persistor = persistStore(store);

export { persistor, store };
