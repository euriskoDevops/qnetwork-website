export default {
  apiUrl: process.env.REACT_APP_API_URL,
  screens: {
    login: '/login',
    home: '/home',
  },
};
