import axios from 'axios';
import constants from './constants';

const baseURL = constants.apiUrl;
const timeout = 60000;
export const publicHttpClient = axios.create({
  baseURL,
  timeout,
});
