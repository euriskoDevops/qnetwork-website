const path = require('path');

module.exports = {
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: {
      '@Images': path.resolve(__dirname, 'src/Assets/Images'),
      '@Styles': path.resolve(__dirname, 'Src/Assets/Styles/'),
      '@Atoms': path.resolve(__dirname, 'src/Components/Atoms/'),
      '@Molecules': path.resolve(__dirname, 'src/Components/Molecules/'),
      '@Organisms': path.resolve(__dirname, 'src/Components/Organisms/'),
      '@Utils': path.resolve(__dirname, 'src/Utils/'),
    },
  },
};
